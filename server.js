var express = require('express');
var app = express();

var PORT = process.env.PORT || 3000;
// require('./app/routes')(app);
app.use(function (req,res,next) {
    console.log(req.method, req.url);
    next()
})
app.all('/*', function (req,res) {
    res.send(`\
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>MEAN webpack application</title>
    </head>
    <body>
        <p>Here we go</p>
    </body>
    </html>
    `)
})

app.listen(PORT, function(){
    console.log('Sever running on port ' + PORT);
})